
import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.Session;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

import java.util.Properties;

/**
 * @author Miroslav Kovachev
 * ${DATE}
 * Methodia Inc.
 */
public class Main {

    public static void main(String[] args) {

        try {

            final Session instance = Session.getInstance(new Properties());
            final Transport transport = instance.getTransport("smtp");
            final MimeMessage mimeMessage = new MimeMessage(instance);
            mimeMessage.addFrom(new Address[] {new InternetAddress("evo.stefanov@methodia.com", "Evo Stefanov")});
            mimeMessage.setSubject("Podaruk");
            mimeMessage.setRecipient(Message.RecipientType.TO,
                    new InternetAddress("miroslav.kovachev@methodia.com", "Miro"));
            mimeMessage.setContent("Zdravei, Sys, na vseki po edin apartament v Dubai! \nPozdravi, Evo",
                    "text/html; charset=utf-8");
            transport.connect("mail.methodia.com", 25, "", "");

            transport.sendMessage(mimeMessage,
                    new Address[] {new InternetAddress("miroslav.kovachev@methodia.com", "Miro")});
            System.out.println("Mail sent!");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}